function fetchAndRenderMenuItems() {
  fetch('menu.json')
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(menuItems => {
      const menuContainer = document.querySelector('.menu-items');
      menuContainer.innerHTML = ''; // Clear existing content
      menuItems.forEach(item => {
        const menuItemHtml = `
                  <div class="card mb-3 col-md-6" id="${item.id}" style="max-width: 660px;">
                      <div class="row g-0">
                          <div class="col-md-4">
                              <img src="${item.image}" class="img-fluid rounded-start" alt="${item.title}">
                          </div>
                          <div class="col-md-8">
                              <div class="card-body">
                                  <h5 class="card-title">${item.title}</h5>
                                  <p class="card-text">${item.description}</p>
                                  <p class="card-text"><small class="text-muted">${item.price}</small></p>
                              </div>
                          </div>
                      </div>
                  </div>
              `;
        menuContainer.innerHTML += menuItemHtml;
      });
    })
    .catch(error => {
      console.error('Error fetching and parsing data:', error);
    });
}

document.addEventListener('DOMContentLoaded', fetchAndRenderMenuItems);


// Function to filter items based on category
function filterMenuItems(categoryId) {
  const items = document.querySelectorAll('.menu-items .card');
  items.forEach(item => {
    if (categoryId === 'all' || item.id === categoryId) {
      item.style.display = 'grid';
    } else {
      item.style.display = 'none';
    }
  });
}

// Event listeners for category buttons
document.querySelector('#breakfastBtn').addEventListener('click', () => filterMenuItems('breakfast'));
document.querySelector('#lunchBtn').addEventListener('click', () => filterMenuItems('lunch'));
document.querySelector('#shakesBtn').addEventListener('click', () => filterMenuItems('shakes'));
document.querySelector('#dinnerBtn').addEventListener('click', () => filterMenuItems('dinner'));
document.querySelector('#allBtn').addEventListener('click', () => filterMenuItems('all'));
